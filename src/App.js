import DataTable from './component/DataTable';

function App() {
  return (
    <div>
      <h2 style={{
        textAlign: 'center'
      }}>{'BẢNG TABLE DATA'}</h2>
      <DataTable></DataTable>
    </div>
  );
}
export default App;
