import {Table, TableCell, TableHead, TableRow, TableContainer, Paper, TableBody, Container, Pagination} from '@mui/material/';
import axios from 'axios';
import { useEffect, useState } from 'react';

function DataTable () {
    const [rows, setRow] = useState([]);
    const [totalPage, setTotalPage] = useState(0);
    const [currentPage, setCurrentPage] = useState(1);
    const numberRow = 3;
 
    const axiosApi = async (config) => {
        const response = await axios(config);
        const data = response;
        return data;
    }

    useEffect(() => {
        let config = {
            method: 'get',
            maxBodyLength: Infinity,
            url: 'https://jsonplaceholder.typicode.com/users',
        };
        axiosApi(config)
            .then((res) => {
                setRow(res.data.slice((currentPage - 1) * numberRow, currentPage * numberRow))
                setTotalPage(Math.ceil(res.data.length/numberRow))                
            console.log(res)
            console.log(currentPage)
        })
            .catch((error) => {
            console.log(error);
        });
    }, [currentPage])

    const handleChangePage = (event, value) => {
        setCurrentPage(value)
    }
    
    return (
        <Container maxWidth={'lg'}>
            <TableContainer component={Paper}>
                <Table sx={{ minWidth: 650 }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                    <TableCell>Id</TableCell>
                    <TableCell align="right">Name</TableCell>
                    <TableCell align="right">Username</TableCell>
                    <TableCell align="right">Email</TableCell>
                    <TableCell align="right">Address</TableCell>
                    <TableCell align="right">Phone</TableCell>
                    <TableCell align="right">Website</TableCell>
                    <TableCell align="right">Company</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {rows.map((row, index) => {
                        return (
                        <TableRow key={index} hover role="checkbox" tabIndex={-1} selected={false} >
                            <TableCell>{row.id}</TableCell>
                            <TableCell align='right'>{row.name}</TableCell>
                            <TableCell align='right'>{row.usename}</TableCell>
                            <TableCell align='right'>{row.email}</TableCell>
                            <TableCell align='right'>{row.address.city} {' '}{row.address.street}{' '}{row.address.suite}{' '}{row.address.zipcode}</TableCell>
                            <TableCell align='right'>{row.phone}</TableCell>
                            <TableCell align='right'>{row.website}</TableCell>
                            <TableCell align='right'>{row.company.name}</TableCell>
                        </TableRow>
                        )
                    })}
                </TableBody>
                </Table>
            </TableContainer>
            <Pagination sx={{
                mt:'2rem'
            }} count={totalPage} onChange={handleChangePage} variant="outlined" shape="rounded" />
        </Container>
    )
}
export default DataTable;